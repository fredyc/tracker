import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import BackgroundGeolocation from 'react-native-background-geolocation';

export default class App extends React.Component {
  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      throw new Error('Location permission not granted');
    }
  }
  componentWillMount() {
    BackgroundGeolocation.configure({
      desiredAccuracy: 0,
      distanceFilter: 1,
      stopTimeout: 1,
      logLevel: BackgroundGeolocation.LOG_LEVEL_VERBOSE,
      url: 'https://requestb.in/uaw5nrua',
      debug: true,
      startOnBoot: false,
      stopOnTerminate: true,
    }, (state) => {
      console.log("- BackgroundGeolocation is configured and ready: ", state.enabled);

      if (!state.enabled) {
        // 3. Start tracking!
        BackgroundGeolocation.start(function() {
          console.log("- Start success");
        });
      }
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>Version 3</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
